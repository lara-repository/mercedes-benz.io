import { test as base } from '@playwright/test';
import { MBWebShopPage } from './MBWebShop';

export const test = base.extend<{ MBWebShop: MBWebShopPage; }>({
  MBWebShop: async ({ page }, use) => {
    const MBShop = new MBWebShopPage(page);
    await use(MBShop);
  },
});