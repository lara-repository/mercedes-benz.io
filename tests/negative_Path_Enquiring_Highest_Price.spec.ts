import { expect } from '@playwright/test';
import { test } from './mb-shop';
import * as fs from 'fs';

async function waitForLoader(page) {
  const loader1 = page.locator('.dcp-loader.dcp-loader--hide');
  await loader1.waitFor({ timeout: 150000 });
};

async function openShopAU(page) {
  await page.goto('https://shop.mercedes-benz.com/en-au/shop/vehicle/srp/demo');
  await page.waitForLoadState();
  await waitForLoader(page);
};

async function acceptCookieBanner(MBWebShop) {
  await MBWebShop.cookieBannerC.waitFor();
  expect(MBWebShop.cookieBannerC).toBeVisible();
  await MBWebShop.acceptAllBtn.click();
};

async function fillLocationModal(page, MBWebShop) {
  await MBWebShop.yourStateLocation.selectOption('New South Wales');
  await MBWebShop.yourPostalCode.fill('2007');
  const purpose =  page.locator('label').filter({ hasText: 'Private' }).locator('div');
  await expect(purpose).toBeVisible();
  await purpose.click();
  await expect(MBWebShop.locationSubmit).toBeVisible();
  await MBWebShop.locationSubmit.click();
};

async function checkSelectedLocation(page) {
  const optionalEnhanceForm = await page.evaluate(() => {
    const optionalEnhanceFormJSON = localStorage.getItem('dcp-mp-au-optionalEnhanceForm');
    return optionalEnhanceFormJSON ? JSON.parse(optionalEnhanceFormJSON) : null;
  });
  const selectedRegion = await page.evaluate(() => localStorage.getItem('dcp-mp-au-selectedRegion'));
  expect(selectedRegion).toBe('New South Wales');
  expect(optionalEnhanceForm.postalCode).toBe('2007');
  expect(optionalEnhanceForm.selectedOption).toBe('P');
};

async function clickFilter(MBWebShop) {
  await MBWebShop.filterToggle.click();
  await expect(MBWebShop.sideBar).toBeVisible();
};

async function filterPreOwnedLink(page, MBWebShop) {
  await MBWebShop.preownedLink.click();
  await waitForLoader(page);
  await expect(page).toHaveURL(/.*used/);
};

async function selectColour(MBWebShop, page) {
  await MBWebShop.filterOpt.getByText('Colour').scrollIntoViewIfNeeded();
  await MBWebShop.filterOpt.getByText('Colour').click();
  await MBWebShop.colourOpener.scrollIntoViewIfNeeded();
  await MBWebShop.colourOpener.click();
  await MBWebShop.colourOptions.getByText(' Cirrus White ', { exact: true }).click();
  await waitForLoader(page);
  await expect(MBWebShop.selectedFiltersWidget).toHaveText('Cirrus White');
  //check results
  const countTile = await MBWebShop.carTile.count();
  const carResultsL = await MBWebShop.carResultsNumber.innerText();
  const carResultsLValue = parseInt(carResultsL.trim());
  expect(carResultsLValue).toEqual(countTile);
}

async function selectHigherPriceTile(page) {
  const priceTile = await page.$$('[data-test-id="dcp-cars-product-tile-price"]');
  let highPrice = 0;
  let indxHighPrice = -1;

  for (let i = 0; i < priceTile.length; i++) {
      const priceText = (await priceTile[i].innerText()).trim();
      const price = parseFloat(priceText.replace('A$', '').replace(',', '').replace('.', ''));
      if (price > highPrice) {
          highPrice = price;
          indxHighPrice = i;
      }
  }

  const higherPriceTile = priceTile[indxHighPrice];
  await higherPriceTile.click();
  await expect(page).toHaveURL(/.*pdp/);
};

async function saveVINandModelYear(MBWebShop, testInfo) {
  const vinValue = await MBWebShop.vinValue.innerText();
  const modelYearValue = await MBWebShop.modelYearValue.innerText();
  const data = {VIN: vinValue,YearModel: modelYearValue};
  const dataJSON = JSON.stringify(data);
  const testName = testInfo.title.replace(/[^a-zA-Z0-9]/g, '_');
  const filePath = `./tests/${testName}_results.json`;
  fs.writeFileSync(filePath, dataJSON);
};

async function enquireAndProceedEmpty(MBWebShop, page) {
  await MBWebShop.enquireBtn.click();
  await waitForLoader(page);
  await expect(MBWebShop.errorMsg).not.toBeVisible();
  await MBWebShop.proceedBtn.click();
  await expect(MBWebShop.errorMsg).toBeVisible();
};

test('Validate the negative path of enquiring the highest price at Mercedes-Benz', async ({ page, MBWebShop }, testInfo) => {

  await openShopAU(page);
  await acceptCookieBanner(MBWebShop);
  await fillLocationModal(page, MBWebShop);
  await checkSelectedLocation(page);
  await clickFilter(MBWebShop);
  await filterPreOwnedLink(page, MBWebShop);
  await clickFilter(MBWebShop);
  await selectColour(MBWebShop, page);
  await selectHigherPriceTile(page);
  await saveVINandModelYear(MBWebShop, testInfo);
  await enquireAndProceedEmpty(MBWebShop, page);

});