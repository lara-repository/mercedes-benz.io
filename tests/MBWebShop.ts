import { type Locator, type Page } from '@playwright/test';

export class MBWebShopPage {
  readonly page: Page;
  readonly cookieBannerC: Locator;
  readonly acceptAllBtn: Locator;
  readonly locationModal: Locator;
  readonly yourStateLocation: Locator;
  readonly yourPostalCode: Locator;
  readonly locationSubmit: Locator;
  readonly filterToggle: Locator;
  readonly sideBar: Locator;
  readonly preownedLink: Locator;
  readonly filterOpt: Locator;
  readonly colourOpener: Locator;
  readonly colourOptions: Locator;
  readonly selectedFiltersWidget: Locator;
  readonly carResultsNumber: Locator;
  readonly carTile: Locator;
  readonly tilePrice: Locator;
  readonly vinValue: Locator;
  readonly modelYearValue: Locator;
  readonly enquireBtn: Locator;
  readonly proceedBtn: Locator;
  readonly errorMsg: Locator;

  constructor(page: Page) {
    this.page = page;
    this.cookieBannerC = page.locator('.cmm-cookie-banner__content');
    this.acceptAllBtn = page.locator('cmm-buttons-wrapper [data-test="handle-accept-all-button"]');
    this.locationModal = page.getByRole('paragraph', {name: 'Colour'});//change
    this.yourStateLocation = page.getByLabel('* Your state');
    this.yourPostalCode = page.locator('[aria-labelledby="postal-code-hint"]');
    this.locationSubmit = page.getByRole('button', {name: 'Continue'});
    this.filterToggle = page.locator('.filter-toggle');
    this.sideBar = page.locator('.sidebar-filter');
    this.preownedLink = page.getByRole('button', {name: ' Pre-Owned'});
    this.filterOpt = page.locator('.category-filter-row-headline');
    this.colourOpener = page.locator('[data-test-id="multi-select-dropdown-card-opener"]').getByText('Colour');
    this.colourOptions =  page.locator('.dcp-multi-select-dropdown-card__pill-wrapper');
    this.selectedFiltersWidget = page.locator('[data-test-id="dcp-selected-filters-widget-tag"]');
    this.carResultsNumber = page.locator('[data-test-id="dcp-cars-srp-result-amount__number"]');
    this.carTile = page.locator('.dcp-cars-srp-results__tile');
    this.tilePrice = page.locator('[data-test-id="dcp-cars-product-tile-price"]');
    this.vinValue = page.locator('.dcp-vehicle-details-list-item__value:below(.dcp-vehicle-details-list-item__label:has-text("VIN"))').first();
    this.modelYearValue = page.locator('.dcp-vehicle-details-list-item__value:below(.dcp-vehicle-details-list-item__label:has-text("Model Year"))').first();
    this.enquireBtn = page.locator('[data-test-id="dcp-buy-box__contact-seller"]');
    this.proceedBtn = page.locator('[data-test-id="dcp-rfq-contact-button-container__button-next"]');
    this.errorMsg = page.locator('.dcp-error-message');
  };

};