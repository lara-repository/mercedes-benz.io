# tech-exercices

## Description
This repository is used for presenting the scenario "Validate the negative path of enquiring the highest price at Mercedes-Benz" for the Mercedes-Benz website in demo environment applying [Playwright](https://playwright.dev/) 🎭 with  using TypeScript.

## Run Playwright E2E-tests

### Install dependencies

Start by cloning the repository and installing the dev dependencies:

```bash
yarn playwright install
```
### Run tests

Use the [Playwright Test for VS Code](https://marketplace.visualstudio.com/items?itemName=ms-playwright.playwright) to run the tests in the tests folder from VS Code or run the following command in the terminal:

```bash
yarn test
```

There is some aditional commands that you can use:
```bash
yarn test-scenarios
yarn report
```

## Contributing

- [] A merge request should be created;
- [] The merge request should be reviewed;


